<?php

namespace App\Repository;

use App\Entity\Scrapper;
use App\Entity\Shop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Scrapper|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scrapper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scrapper[]    findAll()
 * @method Scrapper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScrapperRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Scrapper::class);
    }

    public function findOneByShop(Shop $shop): ?Scrapper
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.shop = :shop')
            ->setParameter('shop', $shop)
            ->orderBy('s.datetimeStart', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
