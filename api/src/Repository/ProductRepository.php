<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\Scrapper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function deleteOldProducts(Scrapper $scrapper): ?int
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.updatedAt < :scrapperStartDate')
            ->andWhere('p.shop = :shop')
            ->setParameter('scrapperStartDate', $scrapper->getDatetimeStart()->modify('-2 day'))
            ->setParameter('shop', $scrapper->getShop())
            ->delete()
            ->getQuery()
            ->execute();
    }
}
