<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Product;
use App\Repository\ScrapperRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class NewProductSubscriber implements EventSubscriberInterface
{
    private $scrapperRepository;
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        ScrapperRepository $scrapperRepository
    )
    {
        $this->scrapperRepository = $scrapperRepository;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['updateScrapper', EventPriorities::POST_WRITE],
        ];
    }

    public function updateScrapper(GetResponseForControllerResultEvent $event)
    {
        $product = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$product instanceof Product && (Request::METHOD_POST !== $method || Request::METHOD_PUT !== $method)) {
            return;
        }

        $scrapper = $this->scrapperRepository->findOneByShop($product->getShop());
        $scrapper->setDatetimeUpdate(new DateTime());
        Request::METHOD_POST === $method
            ? $scrapper->setNewItems($scrapper->getNewItems() + 1)
            : $scrapper->setUpdatedItems($scrapper->getUpdatedItems() + 1);
        $this->entityManager->flush();
    }
}
