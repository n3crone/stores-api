<?php

namespace App\EventSubscriber;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\Scrapper;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class ScrapperEndSubscriber implements EventSubscriberInterface
{
    private $productRepository;
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager,
        ProductRepository $productRepository
    ) {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['deleteOldProducts', EventPriorities::POST_WRITE],
        ];
    }

    public function deleteOldProducts(GetResponseForControllerResultEvent $event)
    {
        $scrapper = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if (!$scrapper instanceof Scrapper || Request::METHOD_PUT !== $method) {
            return;
        }

        $this->productRepository->deleteOldProducts($scrapper);
        $this->entityManager->flush();
    }
}
