<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190331132919 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Initial migration.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE file_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE product_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE scrapper_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE shop_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE file (id INT NOT NULL, content_url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE product (id INT NOT NULL, shop_id INT NOT NULL, image_id INT DEFAULT NULL, base_info VARCHAR(255) NOT NULL, additional_info VARCHAR(255) DEFAULT NULL, normal_price DOUBLE PRECISION NOT NULL, outlet_price DOUBLE PRECISION DEFAULT NULL, quality VARCHAR(255) DEFAULT NULL, discount DOUBLE PRECISION DEFAULT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, link VARCHAR(1000) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D34A04AD4D16C4DD ON product (shop_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD3DA5256D ON product (image_id)');
        $this->addSql('CREATE TABLE scrapper (id INT NOT NULL, shop_id INT NOT NULL, datetime_start TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, datetime_end TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, datetime_update TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, page INT NOT NULL, new_items INT NOT NULL, updated_items INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_8654AA6B4D16C4DD ON scrapper (shop_id)');
        $this->addSql('CREATE TABLE shop (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD3DA5256D FOREIGN KEY (image_id) REFERENCES file (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE scrapper ADD CONSTRAINT FK_8654AA6B4D16C4DD FOREIGN KEY (shop_id) REFERENCES shop (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD3DA5256D');
        $this->addSql('ALTER TABLE product DROP CONSTRAINT FK_D34A04AD4D16C4DD');
        $this->addSql('ALTER TABLE scrapper DROP CONSTRAINT FK_8654AA6B4D16C4DD');
        $this->addSql('DROP SEQUENCE file_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE product_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE scrapper_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE shop_id_seq CASCADE');
        $this->addSql('DROP TABLE file');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE scrapper');
        $this->addSql('DROP TABLE shop');
    }
}
