<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ApiFilter(RangeFilter::class, properties={"normalPrice", "outletPrice"})
 * @ApiFilter(OrderFilter::class, properties={
 *     "discount": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "normalPrice": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "outletPrice": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "baseInfo": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "updatedAt": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "createdAt": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "additionalInfo": {"nulls_comparison": OrderFilter::NULLS_SMALLEST},
 *     "shop.name": {"nulls_comparison": OrderFilter::NULLS_SMALLEST}
 * })
 * @ApiFilter(SearchFilter::class, properties={
 *     "shop": "exact",
 *     "baseInfo": "ipartial",
 *     "additionalInfo": "ipartial",
 *     "link": "exact"
 * })
 */
class Product
{
    /**
     * @var int The entity Id
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @var string Basic name
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    public $baseInfo;

    /**
     * @var string Additional info/Second name row
     *
     * @ORM\Column(type="string", nullable=true)
     */
    public $additionalInfo;

    /**
     * @var float Normal price [PLN]
     *
     * @Groups("shop")
     * @ORM\Column(type="float")
     * @Assert\NotBlank
     */
    public $normalPrice;

    /**
     * @var float Discounted price [PLN]
     *
     * @ORM\Column(type="float", nullable=true)
     */
    public $outletPrice;

    /**
     * @var string Product quality
     *
     * @ORM\Column(type="string", nullable=true)
     */
    public $quality;

    /**
     * @var float Discount percentage
     *
     * @ORM\Column(type="float", nullable=false, options={"default" : 0})
     */
    public $discount = 0;

    /**
     * @var \DateTime Product timestamp
     *
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    public $updatedAt;

    /**
     * @var \DateTime Product timestamp
     *
     * @ORM\Column(type="datetime")
     */
    public $createdAt;

    /**
     * @var Shop Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    /**
     * @ORM\Column(type="string", length=1000)
     * @Assert\NotBlank
     */
    private $link;

    /**
     * @var File|null
     * @ORM\ManyToOne(targetEntity="App\Entity\File")
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    public $image;

    /**
     * @var string
     */
    private $shopName;

    /**
     * @var string
     */
    private $imagePath;

    /**
     * @var string
     */
    private $shopIcon;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBaseInfo(): string
    {
        return $this->baseInfo;
    }

    /**
     * @param string $baseInfo
     */
    public function setBaseInfo(string $baseInfo): void
    {
        $this->baseInfo = $baseInfo;
    }

    /**
     * @return string
     */
    public function getAdditionalInfo(): ?string
    {
        return $this->additionalInfo;
    }

    /**
     * @param string $additionalInfo
     */
    public function setAdditionalInfo(?string $additionalInfo): void
    {
        $this->additionalInfo = $additionalInfo;
    }

    /**
     * @return string
     */
    public function getNormalPrice(): string
    {
        return $this->normalPrice;
    }

    /**
     * @param string $normalPrice
     */
    public function setNormalPrice(string $normalPrice): void
    {
        $this->normalPrice = $normalPrice;
    }

    /**
     * @return string
     */
    public function getOutletPrice(): ?string
    {
        return $this->outletPrice;
    }

    /**
     * @param string $outletPrice
     */
    public function setOutletPrice(?string $outletPrice): void
    {
        $this->outletPrice = $outletPrice;
    }

    /**
     * @return string
     */
    public function getQuality(): ?string
    {
        return $this->quality;
    }

    /**
     * @param string $quality
     */
    public function setQuality(string $quality): void
    {
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function getDiscount(): ?string
    {
        return $this->discount;
    }

    /**
     * @param string $discount
     */
    public function setDiscount(string $discount): void
    {
        $this->discount = $discount;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): void
    {
        $this->link = $link;
    }

    public function getShopName(): string
    {
        return $this->getShop()->getName();
    }

    public function getImagePath(): ?string
    {
        return $this->image ? $this->image->contentUrl : null;
    }

    public function getShopIcon(): ?string
    {
        return $this->shop->favicon ? $this->shop->favicon->contentUrl : null;
    }
}
