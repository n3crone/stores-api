<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 * @ApiFilter(SearchFilter::class, properties={"name": "exact", "shop": "exact"})
 * @ApiResource(
 *     attributes={"security"="is_granted('ROLE_USER')"},
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     normalizationContext={"groups"={"shop"}},
 *     denormalizationContext={"groups"={"shop"}}
 * )
 */
class Shop
{
    /**
     * @Groups("shop")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    public $id;

    /**
     * @Groups("shop")
     * @ORM\Column(type="string", length=255)
     */
    public $name;

    /**
     * @var Product[] Available reviews for this book.
     *
     * @ORM\OneToMany(targetEntity="Product", mappedBy="shop")
     */
    public $products;

    /**
     * @var File|null
     * @Groups("shop")
     * @ORM\ManyToOne(targetEntity="App\Entity\File")
     * @ORM\JoinColumn(nullable=true)
     * @ApiProperty(iri="http://schema.org/image")
     */
    public $favicon;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Scrapper", mappedBy="shop")
     */
    public $scrappers;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->scrappers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setShop($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getShop() === $this) {
                $product->setShop(null);
            }
        }

        return $this;
    }

    /**
     * @Groups("shop")
     */
    public function getImagePath(): ?string
    {
        return $this->favicon ? $this->favicon->contentUrl : null;
    }

    /**
     * @Groups("shop")
     */
    public function getProductsCount(): ?int
    {
        return $this->products ? count($this->products) : null;
    }

    /**
     * @Groups("shop")
     */
    public function getLastScrappedTime(): ?DateTime
    {
        return null;
    }
}
