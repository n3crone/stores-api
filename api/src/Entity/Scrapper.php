<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScrapperRepository")
 * @ApiResource(
 *     attributes={
 *         "security"="is_granted('ROLE_USER')",
 *         "order"={"datetimeStart": "DESC"}
 *     },
 *     collectionOperations={
 *         "get",
 *         "post"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_ADMIN')"},
 *         "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     },
 *     normalizationContext={"groups"={"scrapper"}},
 *     denormalizationContext={"groups"={"scrapper"}}
 * )
 */
class Scrapper
{
    /**
     * @Groups("scrapper")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="datetime")
     */
    private $datetimeStart;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetimeEnd;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetimeUpdate;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="integer")
     */
    private $page;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="integer")
     */
    private $newItems;

    /**
     * @Groups("scrapper")
     * @ORM\Column(type="integer")
     */
    private $updatedItems;

    /**
     * @Groups("scrapper")
     * @ORM\ManyToOne(targetEntity="App\Entity\Shop", inversedBy="scrappers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $shop;

    public function __construct()
    {
        $this->datetimeStart = new DateTime();
        $this->newItems = 0;
        $this->updatedItems = 0;
        $this->page = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDatetimeStart(): ?DateTimeInterface
    {
        return $this->datetimeStart;
    }

    public function setDatetimeStart(DateTimeInterface $datetimeStart): self
    {
        $this->datetimeStart = $datetimeStart;

        return $this;
    }

    public function getDatetimeEnd(): ?DateTimeInterface
    {
        return $this->datetimeEnd;
    }

    public function setDatetimeEnd(?DateTimeInterface $datetimeEnd): self
    {
        $this->datetimeEnd = $datetimeEnd;

        return $this;
    }

    public function getDatetimeUpdate(): ?DateTimeInterface
    {
        return $this->datetimeUpdate;
    }

    public function setDatetimeUpdate(?DateTimeInterface $datetimeUpdate): self
    {
        $this->datetimeUpdate = $datetimeUpdate;

        return $this;
    }

    public function getPage(): ?int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }

    public function getNewItems(): int
    {
        return $this->newItems;
    }

    public function setNewItems(int $newItems): self
    {
        $this->newItems = $newItems;

        return $this;
    }

    public function getUpdatedItems(): ?int
    {
        return $this->updatedItems;
    }

    public function setUpdatedItems(int $updatedItems): self
    {
        $this->updatedItems = $updatedItems;

        return $this;
    }

    public function getShop(): ?Shop
    {
        return $this->shop;
    }

    public function setShop(?Shop $shop): self
    {
        $this->shop = $shop;

        return $this;
    }

    /**
     * @Groups("scrapper")
     */
    public function getShopIcon(): ?string
    {
        return $this->shop->favicon ? $this->shop->favicon->contentUrl : null;
    }

    /**
     * @Groups("scrapper")
     */
    public function getShopName(): string
    {
        return $this->shop->name;
    }
}
