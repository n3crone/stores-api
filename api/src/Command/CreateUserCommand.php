<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class CreateUserCommand extends Command
{
    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    protected static $defaultName = 'create:user';

    private $encoderFactory;
    private $entityManager;
    private $userRepository;

    public function __construct(
        EncoderFactoryInterface $encoderFactory,
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        string $name = null
    ) {
        parent::__construct($name);
        $this->encoderFactory = $encoderFactory;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates user for given credentials.')
            ->addArgument('login', InputArgument::REQUIRED, 'User login.')
            ->addArgument('password', InputArgument::REQUIRED, 'User password.')
            ->addArgument('isAdmin', InputArgument::OPTIONAL, 'Is user admin?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $login = $input->getArgument('login');
        $password = $input->getArgument('password');
        $isAdmin = $input->getArgument('isAdmin');
        $io->note(sprintf('Creating user: %s', $login));

        if ($this->userRepository->findOneBy(['email' => $login])) {
            $io->warning(sprintf('Username already exists.'));

            return;
        }

        $password = $this->encoderFactory->getEncoder('App\Entity\User')->encodePassword($password, null);
        $user = (new User())
            ->setEmail($login)
            ->setPassword($password)
            ->setRoles($isAdmin ? [self::ROLE_USER, self::ROLE_ADMIN] : [self::ROLE_USER]);
        $this->entityManager->persist($user);
        $this->entityManager->flush();

        $io->success('User created!');
    }
}
