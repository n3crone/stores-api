# Dead project - abandoned on successful MVP stage 😞

Idea of project:
- Scrapping shop outlets/CD stores for discounts - due to lack of many on known portals like Ceneo etc. 
- Learn new technologies and microservices

Stack:
- Python (scrapper)
- Angular (front)
- PHP - Symfony/Api Platform (backend)

# Scrapper
Scraps webiste products based on config *.py files. 
AsfaltShop example:
```
CONFIG = {
    const.SHOP_NAME: 'AsfaltShop',
    const.BASE_REQUEST_URL: 'http://asfaltshop.pl/',
    const.SEARCH_URLS: [
        'http://asfaltshop.pl/category/1/?page=%s'
    ],
    const.PAGE: 1,
    const.ROW: 'div.kafelki a',
    const.NAME: {const.SELECTOR: 'div.title div'},
    const.ADDITIONAL_INFO: {
        const.SELECTOR: 'div.title div',
        const.ITERATOR: 1
    },
    const.PRICE_NORMAL: {const.SELECTOR: 'div.pricing'},
    const.PRICE_DISCOUNT: {const.SELECTOR: ''},
    const.QUALITY: {const.SELECTOR: ''},
    const.HREF: {const.ATTRIBUTE: 'href'},
    const.IMAGE: {
        const.SELECTOR: 'img.productThumb',
        const.ATTRIBUTE: 'data-original'
    },
}
```

# API
Persist and serve products with JSON-LD also take control of security, priviliges etc.

Problems:
-  DiscriminatorMap not used which makes DB schema a little dumb.


# Client
Angular + Material design to show products in friendly way.
![](https://scontent.fpoz4-1.fna.fbcdn.net/v/t1.15752-9/92442508_2696359667139373_3805204942512193536_n.jpg?_nc_cat=107&ccb=2&_nc_sid=ae9488&_nc_ohc=LS9YtEIzTRAAX9bs9xA&_nc_ht=scontent.fpoz4-1.fna&oh=496e77d3086a5e2a5e2d7fc0031e7503&oe=6047278A)

Problems:
- awful file structure

# Why dead?
Scrapping situation is complex from law point of view.
